<div id="medias-field-{{ $name }}" class="form-group">
    @if($label)
        <label class="label">{{ $label }}</label>
    @endif
    <media-container/>
</div>

@section('js')
    <script>
        var fieldData = {
            url: '{{ route('media.add') }}',
            csrf: '{{ csrf_token() }}',
            type_id: {{ $type_id }},
            medias: '{!! $images !!}',
            locales: '{!! $locales  !!}',
            multiple: '{{ $multiple }}',
            name: '{{ $name }}',
            dropString: '{{ $dropString }}',
            acceptedFiles: '{{ $accepted }}',
            isImage: true
        };
    </script>
    <script src="{{ asset('vendor/dmedia/js/mediaField.js') }}"></script>
@append
