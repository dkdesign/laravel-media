<?php
return [
    'dropText' => 'Sleep of plaats je :file hier.',
    'image' => 'afbeelding',
    'images' => 'afbeeldingen',
    'document' => 'document',
    'documents' => 'documenten',
    'file' => 'bestand',
    'files' => 'bestanden',
];
