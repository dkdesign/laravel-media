<?php
return [
    'dropText' => 'Drag or click here to upload your :file.',
    'image' => 'image',
    'images' => 'images',
    'document' => 'document',
    'documents' => 'documents',
    'file' => 'file',
    'files' => 'files',
];
