# Toolset for handling media (images and documents) in Laravel.

[![GitHub Tests Action Status](https://img.shields.io/github/workflow/status/spatie/laravel-media/run-tests?label=tests)](https://github.com/Dirk-Kokx/laravel-media/actions?query=workflow%3Arun-tests+branch%3Amaster)


This package makes adding media to your models a breeze.
Just define the different types of media you whish to use and where to put them, add the Trait to the specified Models and you're ready to go!


## Installation

You can install the package via composer:

```bash
composer require deka/media
```


You must publish the assets with:
```bash
php artisan vendor:publish --provider="Deka\Media\MediaServiceProvider" --tag="assets"
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="Deka\Media\MediaServiceProvider" --tag="migrations"
php artisan migrate
```

You should publish the config file with:
```bash
php artisan vendor:publish --provider="Deka\Media\MediaServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
];
```

## Usage

``` php
$laravel-media = new Deka\Media();
echo $media->echoPhrase('Hello, Deka!');
```

## Testing

``` bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email freek@spatie.be instead of using the issue tracker.

## Credits

- [Dirk Kokx](https://github.com/Dirk-Kokx)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
