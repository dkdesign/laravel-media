<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MediaAddParams extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('medias', 'params')) {
            Schema::table('medias', function (Blueprint $table) {
                $table->json('params')->nullable()->after('thumb');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('medias', 'params')) {
            Schema::table('medias', function (Blueprint $table) {
                $table->dropColumn('params');
            });
        }
    }
}
