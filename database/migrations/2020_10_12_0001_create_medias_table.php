<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediasTable extends Migration
{
    public function up()
    {
        Schema::create('medias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('type_id')->default(1);
            $table->string('path');
            $table->json('srcset')->nullable();
            $table->timestamps();
        });

        Schema::create('model_has_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('media_id', false, true);
            $table->string('model_type');
            $table->integer('model_id', false, true);
            $table->string('key')->nullable();
            $table->json('description')->nullable();
            $table->json('alt')->nullable();
            $table->unsignedSmallInteger('ordering')->default(0);

            $table->foreign('media_id')
                ->references('id')
                ->on('medias')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_has_media');
        Schema::dropIfExists('medias');
    }
}
