<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumns extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('medias', 'width')) {
            Schema::table('medias', function (Blueprint $table) {
                $table->unsignedSmallInteger('width')->nullable()->after('path');
                $table->unsignedSmallInteger('height')->nullable()->after('width');
                $table->json('thumb')->nullable()->after('srcset');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('medias', 'width')) {
            Schema::table('medias', function (Blueprint $table) {
                $table->dropColumn('width');
                $table->dropColumn('height');
                $table->dropColumn('thumb');
            });
        }
    }
}
