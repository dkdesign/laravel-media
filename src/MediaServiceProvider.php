<?php

namespace Deka\Media;

use Deka\Media\Commands\CleanMedia;
use Deka\Media\Commands\MediaCommand;
use Deka\Media\Commands\ThumbCommand;
use Deka\Media\Http\Controllers\MediasController;
use Deka\Media\View\Components\InputDocuments;
use Deka\Media\View\Components\InputImages;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class MediaServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/media.php' => config_path('media.php'),
            ], 'config');

            $this->publishes([
                __DIR__.'/../resources/views' => base_path('resources/views/vendor/laravel-media'),
            ], 'views');

            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

            $this->commands([
                MediaCommand::class,
                ThumbCommand::class,
                CleanMedia::class,
            ]);

            $this->publishes([
                __DIR__ . '/../resources/assets' => public_path('vendor/dmedia'),
            ], 'assets');
        }

        Route::macro('dmedia', function (string $prefix) {
            Route::prefix($prefix)->group(function () {
                //media
                Route::post('media/add', [MediasController::class, 'store'])->name('media.add');
                Route::post('media/getall', [MediasController::class, 'get'])->name('media.get');
                Route::post('documents/getall', [MediasController::class, 'getDocuments'])->name('documents.get');

            });
        });

        $this->loadTranslationsFrom(__DIR__. '/../resources/lang', 'dmedia');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'dmedia');

        $this->loadViewComponentsAs('dmedia', [
            'input-images' => InputImages::class,
            'input-documents' => InputDocuments::class,
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/media.php', 'media');
        config(['media.locales' => config('app.locales', [config('app.locale', 'nl')])]);
    }
}
