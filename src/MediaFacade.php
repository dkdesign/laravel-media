<?php

namespace Deka\Media;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Deka\Media\Media
 */
class MediaFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'media';
    }
}
