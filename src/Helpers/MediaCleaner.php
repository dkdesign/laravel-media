<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 2019-08-16
 * Time: 07:59
 *
 * Schoont de bestanden en records op van medias die;
 * - niet in de media-tabel voorkomen
 * - wel in de tabel voorkomen maar geen referentie hebben vamuit gebruikerstabellen
 */

namespace Deka\Media\Helpers;

use Deka\Media\Models\Media;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class MediaCleaner
{
    protected $type_id;
    protected $type;
    protected $disk;
    protected $path;

    /**
     * MediaCleaner constructor.
     * @param $type (array as defined in config.media.types)
     */
    public function __construct(array $type)
    {
        $this->type_id = $type['type_id'];
        $this->type = $type;
        $this->disk = $type['disk'];
        $this->path = $type['path'];
    }

    public function removeUnlinked()
    {
        $docs = $this->getFiles();

        foreach ($docs as $doc) {
            if (!Media::where([['type_id', $this->type_id], ['path', $doc]])->count()) {
                Storage::disk($this->disk)->delete($doc);
            }
        }
    }

    private function getFiles()
    {
        $directory = $this->path;
        return array_filter(Storage::disk($this->disk)->files($directory), function ($file) use ($directory) {
            $name = substr($file, strlen($directory) + 1);
            if (substr($name, 0, 1) == '.')
                return false;

            return true;
        });
    }

    /**
     * removes thumbs in usermedia, always starting with 205-
     */
    protected function removeFile($path)
    {
        if(Storage::disk($this->disk)->exists($path))
            Storage::disk($this->disk)->delete($path);
    }

    public function removeUnused()
    {
        if($this->type['delete_unused']) {
            $used_ids = $this->type['model']::whereNotNull($this->type['column'])->pluck($this->type['column'])->toArray();
            $all_ids = Media::where('type_id', $this->type_id)->pluck('id')->toArray();
            $unused = array_diff($all_ids, $used_ids);

            foreach ($unused as $id) {
                $medium = Media::find($id);

                //only delete mediums older than two hours
                $now = Carbon::now(config('app.timezone'));
                if ($medium->created_at->addHours(2)->lessThan($now)) {
                    if ($medium->thumb)
                        $this->removeFile($medium->thumb['path']);

                    if ($medium->srcset) {
                        foreach ($medium->srcset as $path) {
                            $this->removeFile($path);
                        }
                    }

                    $this->removeFile($medium->path);
                    $medium->delete();
                }
            }
        }
    }
}
