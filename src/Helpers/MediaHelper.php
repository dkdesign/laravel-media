<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 2019-01-20
 * Time: 16:19
 */

namespace Deka\Media\Helpers;

use Deka\Media\Models\Media;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Exception\NotFoundException;
use Intervention\Image\Facades\Image;

class MediaHelper
{

    public static function getExtensionFromMime($mime)
    {
        switch ($mime) {
            case 'application/pdf':
                return 'pdf';
            case substr($mime, 0, 5) === 'image':
                return substr($mime, 6);
            case substr($mime, 0, 5) === 'video':
                return substr($mime, 5);
            case 'application/msword':
                return 'doc';
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                return 'docx';
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                return 'xlsx';
            case 'application/vnd.ms-excel':
                return 'xls';
            default:
                return 'unknown';
        }
    }

    public static function getType(int $typeId)
    {
        $types = config('media.types');
        $type = collect($types)->where('type_id', $typeId)->first();

        return ($type)?? false;
    }

    public static function createSourceSet(Media $media)
    {
        $type = $media->getType();
        $iImage = Image::make($media->getUrl());

        $srcset = [$iImage->width() => $media->path];

        if($iImage->mime() !== 'image/gif') {
            $dirname = explode('.', $media->getFilename())[0];
            $basepath = $type['path'].'/'.$dirname;

            self::setDirectoryIfNotExists($type, $dirname);

            while($iImage->width() > $type['min_width']){
                $newWidth = (int) ($iImage->width() * $type['downsize_ratio']);
                if($newWidth < $type['min_width'])
                    break;

                $extension = config('media.image_format', 'jpg');

                $name = $dirname.'-'.$newWidth;
                $fullpath = $basepath . "/{$name}.{$extension}";

                $iImage = $iImage->resize($newWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                Storage::disk($type['disk'])->put($fullpath, $iImage->encode($extension, 100));

                $srcset[$newWidth] = $fullpath;
            }
        }

        $iImage->destroy();

        return $srcset;
    }

    public static function createThumb(Media $media): array
    {
        $type = $media->getType();
        $format = $type['thumb_format'] ?? config('media.image_format');

        if (
            ! isset($type['thumb_width']) &&
            ! isset($type['thumb_height'])
        ) {
            $width = $type['min_width'] ?? 150;
            $height = null;
        } else {
            $width = $type['thumb_width'] ?? null;
            if ($width === 'auto') $width = null;
            $height = $type['thumb_height'] ?? null;
            if ($height === 'auto') $height = null;
        }

        $iImage = Image::make($media->getUrl());

        if (!$iImage) {
            throw new NotFoundException('Could not get image from url '.$media->getUrl());
        }

        if (! $media->width) $media->width = $iImage->width();
        if (! $media->height) $media->height = $iImage->height();

        $thumb = [];

        if ($width && $height) {
            $iImage = $iImage->resize($width, $height, function ($constraint) {
                $constraint->upsize();
            });
        } else {
            $iImage = $iImage->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $dirname = explode('.', $media->getFilename())[0];
        $basepath = $type['path'].'/'.$dirname;
        self::setDirectoryIfNotExists($type, $dirname);

        $name = $dirname.'-thumb';
        $fullpath = $basepath . "/{$name}.{$format}";
        Storage::disk($type['disk'])->put($fullpath, $iImage->encode($format, 100));

        $thumb['path'] = $fullpath;
        $thumb['width'] = $iImage->width();
        $thumb['height'] = $iImage->height();

        $media->thumb = $thumb;
        $media->save();

        return $thumb;
    }

    protected static function setDirectoryIfNotExists($type, $directory)
    {
        if (! File::isDirectory(Storage::disk($type['disk'])->path($type['path'].'/'.$directory))) {
            // path does not exist
            File::makeDirectory(Storage::disk($type['disk'])->path($type['path'].'/'.$directory), '0755', true);
        }
    }

}
