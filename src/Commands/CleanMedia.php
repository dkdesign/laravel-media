<?php

namespace Deka\Media\Commands;

use Deka\Media\Helpers\MediaCleaner;
use Deka\Media\Helpers\MediaHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CleanMedia extends Command
{
    protected $disk;
    protected $path;

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'media:clean {type?}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Removes all removable unused media for all or an optional type';

    /**
     * Create a new command instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        Log::channel('command')->info('cronjob CleanMedia called');

        $typeId = (int) $this->argument('type');

        if ($typeId > 0) {
            $type = MediaHelper::getType($typeId);
            if ($type) {
                $this->deleteMediaForType($type);
            }
        } else {
            $types = config('media.types');
            foreach ($types as $type) {
                $this->deleteMediaForType($type);
            }

        }

        Log::channel('command')->info('cronjob CleanMedia completed');
    }

    public function deleteMediaForType(array $type): void
    {
        if ($type['delete_unused']) {
            $cleaner = new MediaCleaner($type);
            $cleaner->removeUnused();
            $cleaner->removeUnlinked();
        }
    }

}
