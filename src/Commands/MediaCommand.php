<?php

namespace Deka\Media\Commands;

use Deka\Media\Helpers\MediaHelper;
use Deka\Media\Models\Media;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class MediaCommand extends Command
{
    public $signature = 'media:create-sets {type?} {--new-only}';

    public $description = 'Creates sourceSets for all responsive images or for the specified typeId';

    public function handle()
    {
        $typeId = (int) $this->argument('type');
        $newOnly = $this->option('new-only');

        if ($typeId > 0) {
            $query = Media::where('type_id', $typeId);
        } else {
            $typeIds = collect(config('media.types'))->where('is_responsive', true)->pluck('type_id')->toArray();

            $query = Media::whereIn('id', $typeIds);
        }

        if($newOnly) {
            $query->where('srcset', '')->orWhereNull('srcset');
        }

        $medias = $query->get();

        if ($medias) {
            foreach ($medias as $media) {
                try {
                    $set = MediaHelper::createSourceSet($media);
                } catch (\Exception $e) {
                    Log::error('SrccSet creation failed: '. $e->getMessage());
                    continue;
                }
                $media->update(['srcset' => $set]);
            }
        }
        $this->comment('All done');
    }
}
