<?php

namespace Deka\Media\View\Components;


class InputDocuments extends \Illuminate\View\Component
{

    public $label;
    public $name;
    public $dropString;
    public $medias;
    public $type_id;
    public $locales;
    public $multiple;
    public $accepted;

    public function __construct(
        int $typeId,
        $name,
        array $medias = [],
        array $locales = [],
        bool $multiple = false,
        $label = false,
        $dropString = false,
        $accepted = '.doc, .docx, .dotx, .pdf, .zip, .xls, .xlsx, .xlsm'
    )
    {
        $this->name = $name;
        $this->label = $label;
        $this->medias = json_encode($medias);
        $this->type_id = $typeId;
        $this->locales = (count($locales) < 1)? json_encode(config('media.locales')) : json_encode($locales);
        $this->multiple = $multiple;
        $this->accepted = $accepted;

        $this->setDropString($dropString, $multiple);
    }

    private function setDropString($dropString, $multiple)
    {
        if ($dropString) {
            $this->dropString = $dropString;
        } else {
            if ($multiple) {
                $file = __('dmedia::media.documents');
            } else {
                $file = __('dmedia::media.document');
            }
            $this->dropString = __('dmedia::media.dropText', ['file' => $file]);
        }
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        return view('dmedia::components.input-documents');
    }
}
