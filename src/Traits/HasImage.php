<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 13/07/2020
 *
 * Deze Trait is zinvol om te gebruiken in een model bij een many to many relatie met de medias tabel
 * De trait update de relatie-tabel bij het aanmaken, updaten of verwijderen van de record
 * Ook is het mogelijk om in de relatietabel titel en omschrijving (caption) mee te geven aan een afbeelding
 *
 * A callback can be used by defining a onAfterMediaSet function on the model
 */

namespace Deka\Media\Traits;

use Deka\Media\Models\Media;

trait HasImage
{
    /**
     * should return an array of input fields with the following data per field:
     * ['name' (name of input), 'type' (image or doc), 'key' (optional to label different medias - i.e. a header image or a content image)
     * @return array
     */
    abstract public function getImageColumn(): string;
    abstract public function getFormFieldNames(): array;

    public static function bootHasImage()
    {
        static::saving(function ($model) {
            $model->setImage($model, request()->toArray());
            if (method_exists($model, 'onAfterImageSet'))
                $model->onAfterImageSet();
        });
    }

    /**
     * Assign the given media to the model.
     * @param array $input
     */
    public function setImage($model, $input)
    {
        $fieldNames = $this->getFormFieldNames();

        foreach ($fieldNames as $fieldName) {
            if (isset($input[$fieldName])) {
                $medias = json_decode($input[$fieldName], 1);

                if (count($medias)) {
                    foreach ($medias as $key => $media) {
                        if ($media['id'] < 1) {
                            unset($medias[$key]);
                        } else {
                            $image = Media::find($media['id']);
                            if ($image) {
                                $data = [
                                    'description' => null,
                                    'alt' => null,
                                ];

                                if (isset($media['description'])) {
                                    if (is_array($media['description'])) {
                                        $data['description'] = $media['description'];
                                    } else if ($media['description'] !== '') {
                                        //string only, we convert it to an array with the string as value
                                        $data['description'] = [$media['description']];
                                    }
                                }

                                if (isset($media['alt'])) {
                                    if (is_array($media['alt'])) {
                                        $data['alt'] = $media['alt'];
                                    } else if ($media['alt'] !== '') {
                                        //string only, we convert it to an array with the string as value
                                        $data['alt'] = [$media['alt']];
                                    }
                                }

                                $image->update(['params' => $data]);
                                $col = $this->getImageColumn();
                                $model->$col = $media['id'];
                            }
                        }
                    }
                } else {
                    $col = $this->getImageColumn();
                    $model->$col = null;
                }
            }
        }
    }

    public function getImageData($fieldName)
    {
        if ($image = $this->image($fieldName)->first()) {
            return [
                [
                    'id' => $image->id,
                    'url' => $image->getUrl(),
                    'description' => $this->getParam($image, 'description'),
                    'alt' => $this->getParam($image, 'alt'),
                    'width' => $image->width,
                    'height' => $image->height,
                ]
            ];
        }

        return [];
    }

    private function getParam($image, $name, $default = '')
    {
        if ($image->params && isset($image->params[$name]))
            return $image->params[$name];

        return $default;
    }

    /**
     * associations
     */
    public function image()
    {
        return $this->belongsTo(
            'Deka\Media\Models\Media',
            $this->getImageColumn()
        );
    }
}
