<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 13/07/2020
 *
 * Deze Trait is zinvol om te gebruiken in een model bij een many to many relatie met de medias tabel
 * De trait update de relatie-tabel bij het aanmaken, updaten of verwijderen van de record
 * Ook is het mogelijk om in de relatietabel titel en omschrijving (caption) mee te geven aan een afbeelding
 *
 * A callback can be used by defining a onAfterMediaSet function on the model
 */

namespace Deka\Media\Traits;

use Deka\Media\Models\ModelHasMedia;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait HasMedia
{
    /**
     * should return an array of input fields with the following data per field:
     * ['name' (name of input), 'type' (image or doc), 'key' (optional to label different medias - i.e. a header image or a content image)
     * @return array
     */
    abstract public function getMediaFields(): array;

    public static function bootHasMedia()
    {
        static::saved(function ($model) {
            $model->setMedia(request()->toArray());
            if (method_exists($model, 'onAfterMediaSet'))
                $model->onAfterMediaSet();
        });

        static::deleting(function ($model) {
            $model->media()->detach();
        });
    }

    /**
     * Assign the given media to the model.
     * @param array $input
     */
    public function setMedia($input)
    {
        foreach ($this->getMediaFields() as $mediaField) {
            if (isset($input[$mediaField['name']])) {
                $medias = json_decode($input[$mediaField['name']], 1);
                $i = 1;
                foreach ($medias as $key => $media) {
                    if ($media['id'] < 1) {
                        unset($medias[$key]);
                    } else {
                        $data = [
                            'description' => null,
                            'alt' => null,
                            'ordering' => $i
                        ];

                        if (isset($media['description'])) {
                            if (is_array($media['description'])) {
                                $data['description'] = $media['description'];
                            } else if ($media['description'] !== '') {
                                //string only, we convert it to an array with the string as value
                                $data['description'] = [$media['description']];
                            }
                        }

                        if ($mediaField['type'] === 'image') {
                            if (isset($media['alt'])) {
                                if (is_array($media['alt'])) {
                                    $data['alt'] = $media['alt'];
                                } else if ($media['alt'] !== '') {
                                    //string only, we convert it to an array with the string as value
                                    $data['alt'] = [$media['alt']];
                                }
                            }
                        }

                        if (isset($mediaField['key']))
                            $data['key'] = $mediaField['key'];

                        ModelHasMedia::updateOrCreate(
                            ['media_id' => $media['id'], 'model_id' => $this->id, 'model_type' => self::class],
                            $data
                        );
                    }
                    $i++;
                }
            }

            //remove 'deleted' media
            if (isset($input[$mediaField['name'].'_deleted'])) {
                $deletables = json_decode($input[$mediaField['name'].'_deleted'], 1);
                ModelHasMedia::whereIn('media_id', $deletables)->where([['model_id', $this->id], ['model_type', self::class]])->delete();
            }
        }
    }

    public function hasMedia() : bool
    {
        return ($this->media->count() > 0);
    }

    public function getMedia($type_id, $key = false)
    {
        if ($key) {
            $medias = $this->media()->where('type_id', $type_id)->wherePivot('key', $key)->orderBy('pivot_ordering')->get();
        } else {
            $medias = $this->media()->where('type_id', $type_id)->whereNull('key')->orderBy('pivot_ordering')->get();
        }

        return $medias->map(function ($medium) {
            return [
                'id' => $medium->id,
                'url' => $medium->getUrl(),
                'description' => json_decode($medium->pivot->description),
                'alt' => json_decode($medium->pivot->alt),
            ];
        })->toArray();

    }

    public function getMainImageAttribute()
    {
        if ($this->images->count() > 0) {
            return $this->images->first();
        }

        return false;
    }

    /**
     * associations
     */
    public function media(): MorphToMany
    {
        return $this->morphToMany(
            'Deka\Media\Models\Media',
            'model',
            'model_has_media',
            'model_id',
            'media_id'
        )
            ->withPivot('ordering', 'alt', 'description', 'key');
    }

    public function images()
    {
        return $this->media()->whereNotIn('type_id', config('media.doctypes'))->orderBy('pivot_ordering');
    }

    public function files()
    {
        return $this->media()->whereIn('type_id', config('media.doctypes'))->orderBy('pivot_ordering');
    }
}
