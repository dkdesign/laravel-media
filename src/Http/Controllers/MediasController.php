<?php

namespace Deka\Media\Http\Controllers;

use Deka\Media\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class MediasController extends Controller
{

    public function get($type_id)
    {
        $images = Media::where('type_id', $type_id)->get()->shuffle();
        $type = collect(config('media.types'))->where('type_id', $type_id)->first();

        return $this->returnJsonImages($images, $type['disk']);
    }

    protected function returnJsonImages($images, $disk)
    {
        $medias = [];
        foreach ($images as $image) {
            $medias[] = [
                'name' => $image->getFilename(),
                'link' => Storage::disk($disk)->url($image->path),
                'id' => $image->id
            ];
        }

        return json_encode(['images' => $medias]);
    }

    protected function returnJsonDocs($docs)
    {
        $medias = [];
        foreach ($docs as $doc) {
            $medias[] = [
                'name' => $doc->getFilename(),
                'link' => asset('img/file-icon.png'),
                'id' => $doc->id
            ];
        }

        return json_encode(['images' => $medias]);
    }

    public function getDocuments()
    {
        $docs = Media::where('type_id', 1)->get()->sortBy('path');
        return $this->returnJsonDocs($docs);
    }

    public function index()
    {
        $images = Media::where('type_id', 2)->get();
        return view('admin.medias.index', compact('images'));
    }

    public function store(Request $request)
    {
        $typeId = $request->get('type_id', false);
        $mimes = false;
        if ($typeId) {
            $type = collect(config('media.types'))->where('type_id', $typeId)->first();
            if ($type && isset($type['mime_types'])) {
                $mimes = $type['mime_types'];
            }
        }

        if(!$mimes) {
            $mimes = 'jpg,jpeg,png,bmp,gif,pdf,docx,doc,zip';
        }

        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:'.$mimes,
            'type_id' => 'required|integer'
        ]);

        if($validator->fails())
            return json_encode(['link' => "", 'media_id' => null, 'error' => 'not valid']);

        $file = $request->file('file');
        $input = $request->all();
        $type_id = $input['type_id'];

        $type = collect(config('media.types'))->where('type_id', $type_id)->first();

        if (! $type) {
            return json_encode(['link' => "", 'media_id' => null, 'error' => 'type not found']);
        }

        if(! isset($type['allow_override']) || ! $type['allow_override']){
            $name = $file->getClientOriginalName();
        } else {
            $name = $this->getUniqueName($type['path'], $file->getClientOriginalName(), $type['disk'], false);
        }

        $path = $this->saveFile($file, $type, $name);
        if(! $path)
            return json_encode(['link' => "", 'media_id' => null, 'error' => $this->media_error]);

        $media = Media::create([
            'type_id' => $type_id,
            'path' => $path,
        ]);

        $url = Storage::disk($type['disk'])->url($path);

        return response()->json(
            ["success" => true, "url" => $url, 'media_id' => $media->id]
        );
    }

    protected function saveFile($file, $type, $source_name)
    {
        $change_name = false;
        $path = $type['path'];

        $name = $this->getUniqueName($path, $source_name, $type['disk'], $change_name);

        if($type['mediatype'] === 'image') {
            try {
                $iImage = Image::make($file);
                $mime = $iImage->mime();
                if($mime !== 'image/gif'){
                    if($iImage->width() > $type['max_width']) {
                        //we downsize all images to a max
                        $sharpening_amount = $this->getSharpeningAmount($iImage, $type['max_width'], null);
                        $resized = $iImage->resize($type['max_width'], null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        if($sharpening_amount > 0)
                            $iImage->sharpen($sharpening_amount);

                        $iImage->encode('jpg', 90);
                    } else {
                        $resized = $iImage->encode('jpg', 90);
                    }
                    Storage::disk($type['disk'])->put($path . '/' . $name, $resized);
                } else {
                    //we store gifs without modification
                    Storage::disk($type['disk'])->put($path.'/'.$name, file_get_contents($file));
                }

            } catch (\Exception $e) {
                $this->media_error = $e->getMessage();
                return false;
            }
        } else {
            Storage::disk($type['disk'])->put($path.'/'.$name, file_get_contents($file));
        }

        return $path.'/'.$name;
    }

    /**
     * calculates the amount of sharpening needed when downsizing
     * @param Intervention Image $img
     * @param $width
     * @param $height
     * @return float|int
     */
    protected function getSharpeningAmount($img, $width, $height)
    {
        $amount = 0;

        //sharpen, depending on the amount of minification
        if($width < 0.9 * $img->width() || $height < 0.9 * $img->height() ){
            $baseamount = 5;

            if($width){
                $amount =  (int) $img->width() / $width;
                if($amount > $baseamount)
                    $baseamount = $amount;
            }

            if($height){
                $amount =  (int) $img->height() / $height;
                if($amount > $baseamount)
                    $baseamount = $amount;
            }

            $amount = ($baseamount > 100)? 100 : $baseamount;
        }

        return $amount;
    }

    protected function getUniqueName($path, $preferred, $disk, $change_name = false)
    {
        $fragments = explode('.', $preferred);
        $filetype = false;
        if(count($fragments) > 1){
            $filetype = array_pop($fragments);
        }

        if($change_name){
            $body = Str::random(24);
        } else {
            $body = implode($fragments);
            $body = Str::slug($body);
        }
        $name = ($filetype)? $body.'.'.$filetype : $body;

        $i = 1;
        while(Storage::disk($disk)->exists($path.'/'.$name)){
            $name = $body.'-'.$i;
            if($filetype) $name .= '.'.$filetype;
            $i++;
        }

        return $name;
    }

    public function destroy($id)
    {
        Media::destroy($id);
        return redirect()->route('admin.medias.index')->with('info', 'De afbeelding is verwijderd.');
    }

    public function deleteHeaderPhoto($params = false)
    {
        if(! $params)
            abort('404');

        Media::destroy($params);
    }

}
