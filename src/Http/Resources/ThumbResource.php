<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 08/11/2020
 * Time: 20:59
 */

namespace Deka\Media\Http\Resources;

use Deka\Media\Transformers\ThumbTransformer;
use Illuminate\Http\Resources\Json\JsonResource;

class ThumbResource extends JsonResource
{
    public function toArray($request)
    {
        return ThumbTransformer::getInstance()->transform($this->resource);
    }
}
