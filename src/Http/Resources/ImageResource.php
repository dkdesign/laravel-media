<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 08/11/2020
 * Time: 20:59
 */

namespace Deka\Media\Http\Resources;

use Deka\Media\Transformers\ImageTransformer;
use Illuminate\Http\Resources\Json\JsonResource;

class ImageResource extends JsonResource
{
    public function toArray($request)
    {
        return ImageTransformer::getInstance()->transform($this->resource);
    }
}
