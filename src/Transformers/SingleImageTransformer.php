<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 05/06/2021
 * Time: 15:45
 */

namespace Deka\Media\Transformers;


use Deka\Media\Models\Media;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SingleImageTransformer
{
    private static $instance;

    private function __construct() {}

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new SingleImageTransformer();
        }

        return self::$instance;
    }

    public function transform($image): array
    {
        if ($image instanceof Media) {
            try {
                $imgData = [
                    'path' => $image->getUrl(),
                    'thumb' => $image->getThumbPath(),
                    'srcset' => $image->getSrcset(true),
                    'width' => $image->width,
                    'height' => $image->height,
                    'sizes' => config('media.sizes', false)
                ];

                $imgData['url'] = $imgData['path'];

                return $imgData;
            } catch (\Exception $e) {
                Log::error($e->getMessage(), $e->getTrace());
                return $this->getFallbackImage();
            }
        } else {
            return $this->getFallbackImage();
        }
    }

    public function collection($images)
    {
        $data = [];
        foreach ($images as $image) {
            $data[] = $this->transform($image);
        }

        return $data;
    }

    public function getFallbackImage(): array
    {
        $image = config('media.fallback_image');

        return [
            'path' => Storage::disk($image['disk'])->url($image['path']),
            'thumb' => Storage::disk($image['disk'])->url($image['thumb_path']),
            'alt' => null,
            'caption' => null,
            'ordering' => 1,
        ];
    }
}
