<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 05/06/2021
 * Time: 15:45
 */

namespace Deka\Media\Transformers;


use Deka\Media\Models\Media;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DocumentTransformer
{
    private static $instance;

    private function __construct() {}

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new DocumentTransformer();
        }

        return self::$instance;
    }

    public function transform($doc): array
    {
        if ($doc instanceof Media) {
            try {
                $docData = [
                    'path' => $doc->getUrl(),
                    'url' => $doc->path,
                ];

                return $docData;
            } catch (\Exception $e) {
                Log::error($e->getMessage(), $e->getTrace());
            }
        }

        return [];
    }

    public function collection($docs)
    {
        $data = [];
        foreach ($docs as $doc) {
            $data[] = $this->transform($doc);
        }

        return $data;
    }
}
