<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 05/06/2021
 * Time: 15:45
 */

namespace Deka\Media\Transformers;


use Deka\Media\Models\Media;
use Illuminate\Support\Facades\Storage;

class ImageSitemapTransformer
{
    private static $instance;

    private function __construct() {}

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new ImageSitemapTransformer();
        }

        return self::$instance;
    }

    public function transform($image): array
    {
        if ($image instanceof Media) {
            $data = [
                'url' => $image->getUrl(),
            ];

            if (! empty($image->pivot->alt)) {
                $data['title'] = $image->pivot->alt;
            }

            if (! empty($image->pivot->description)) {
                $data['caption'] = $image->pivot->description;
            }
        } else {
            $image = config('media.fallback_image');

            $data = [
                'url' => Storage::disk($image['disk'])->url($image['path']),
                'alt' => null,
                'caption' => null,
            ];
        }

        return $data;
    }

    public function collection($images)
    {
        $data = [];
        foreach ($images as $image) {
            $data[] = $this->transform($image);
        }

        return $data;
    }
}
