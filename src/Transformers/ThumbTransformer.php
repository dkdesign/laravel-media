<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 05/06/2021
 * Time: 15:45
 */

namespace Deka\Media\Transformers;


use Deka\Media\Models\Media;
use Illuminate\Support\Facades\Storage;

class ThumbTransformer
{
    private static $instance;

    private function __construct() {}

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new ThumbTransformer();
        }

        return self::$instance;
    }

    public function transform($image): array
    {
        $thumb = ($image instanceof Media)? $image->getThumb() : false;

        if ($thumb) {
            return [
                'path' => Storage::disk($image->getType()['disk'])->url($thumb['path']),
                'alt' => $image->pivot->alt,
                'width' => $thumb['width'],
                'height' => $thumb['height'],
                'caption' => $image->pivot->description,
                'ordering' => $image->pivot->ordering,
            ];
        } else {
            $image = config('media.fallback_image');

            return [
                'path' => Storage::disk($image['disk'])->url($image['thumb_path']),
                'alt' => null,
                'caption' => null,
                'ordering' => 1,
            ];
        }
    }

    public function collection($images)
    {
        $data = [];
        foreach ($images as $image) {
            $data[] = $this->transform($image);
        }

        return $data;
    }
}
