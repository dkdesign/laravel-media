<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 11/07/2020
 * Time: 09:00
 */

namespace Deka\Media\Models;

use Deka\Media\Helpers\MediaHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Media extends Model
{
    protected $fillable = ['path', 'type_id', 'srcset', 'width', 'height', 'thumb', 'params'];
    protected $table = 'medias';
    protected $casts = [
        'srcset' => 'array',
        'thumb' => 'array',
        'params' => 'array',
    ];

    public function getUrl()
    {
        try {
            return Storage::disk($this->getType()['disk'])->url($this->path);
        } catch(\Exception $e){
            return false;
        }
    }

    public function getThumb()
    {
        if ($this->thumb) {
            return $this->thumb;
        }

        try {
            return MediaHelper::createThumb($this);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getThumbPath()
    {
        $thumb = $this->getThumb();
        if ($thumb) {
            return Storage::disk($this->getType()['disk'])->url($thumb['path']);
        }

        return $this->path;
    }

    public function getFilename()
    {
        $parts = explode('/', $this->path);
        return array_pop($parts);
    }

    /**
     * returns a html string of the sourceset
     * if none is available it will be created if the given type allows
     * otherwise it will return false
     */
    public function getSrcset($to_html = true)
    {
        $type = $this->getType();
        if(is_null($this->srcset)){
            if($type['is_responsive']){
                $srcset = $this->setSrcset();
            } else {
                return false;
            }
        } else {
            $srcset = $this->srcset;
        }

        if($srcset && $to_html) {
            return $this->srcsetToHtml($srcset, $type['disk']);
        }
        return $srcset;
    }

    public function srcsetToHtml($srcset, $disk = 'public')
    {
        $html = '';
        $i = 1;
        foreach ($srcset as $width => $path) {
            if($i > 1)
                $html .= ', ';
            $html .= Storage::disk($disk)->url($path).' '.$width.'w';
            $i ++;
        }
        return $html;
    }

    public function setSrcset()
    {
        $srcset = MediaHelper::createSourceSet($this);

        if ($srcset) {
            $this->srcset = $srcset;
            $this->save();
        }

        return $srcset;
    }

    public function getType()
    {
        return collect(config('media.types'))->where('type_id', $this->type_id)->first();
    }

}

