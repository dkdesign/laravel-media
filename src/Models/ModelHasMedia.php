<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 13/07/2020
 * Time: 23:18
 */

namespace Deka\Media\Models;


use Illuminate\Database\Eloquent\Model;

class ModelHasMedia extends Model
{
    protected $fillable = ['media_id', 'model_type', 'model_id', 'description', 'alt', 'ordering', 'key'];
    public $timestamps = false;
    protected $casts = ['description' => 'array', 'alt' => 'array'];
}
