# Changelog

All notable changes to `laravel-media` will be documented in this file

## 0.0.1 - 2020-07-10

- initial release
