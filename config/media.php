<?php

return [
    /**
     * define the various media needed and what there location should be
     * disk - name of the disk defined in filesystems
     * path - relative path in the defined disk. BEWARE TO GIVE EACH TYPE ITS OWN FOLDER, TO AVOID DELETION BY THE MEDIACLEANER
     * mediatype - one of ['image', 'document']
     * model - model that references the media
     * on models using the many-to-many trait HasMedia use model '\Deka\Media\
     * column - column in the specified model that references the id of the medias table
     * delete_unused - tell the CleanMedia command to remove media if they are not linked to the specified model
     * allow_override - setting this to true will override any existing file on that location with the same name
     *
     * for document types:
     * accepted_files - comma seperated file extensions that are allowed
     * mime_types: - comma seperated file extensions that are allowed (validated on saving the file)
     *
     * for the image type:
     * max_width - max width the uploaded image should be stored in. On storing larger images will be downsized
     * is_response - when set to true the package will create smaller variants of the image for use on various screensizes
     *
     * for responsive images
     * min_width - smallest image size to be rendered
     * downsize_ratio - amount of downsizing each time when creating smaller images
     * Example an image with max_width 1000, min_width 125 and ratio 0.5 will have images with width-sizes 1000, 500, 250 and 125
     *
     * An image has a seperate thumb generated with the following params
     * thumb_width - size in pixels or 'auto'
     * thumb_height - size in pixels or 'auto' (width or height should be defined in pixels, setting both will potentially crop the image)
     * thumb_format - image format of the thumb
     * if none are specified the default format is used, and the min_width will be used as width (or a fallback width of 150 pixels).
     */
    'types' => [
        0 => [
            'type_id' => 1,
            'disk' => 'public',
            'path' => 'manuals',
            'mediatype' => 'document',
            'accepted_files' => '.doc, .docx, .dotx, .pdf, .zip, .xls, .xlsx, .xlsm',
            'mime_types' => 'jpg,jpeg,png,bmp,gif,pdf,docx,doc,zip',
            'model' => \Deka\Media\Models\ModelHasMedia::class,
            'column' => 'media_id',
            'delete_unused' => true,
            'allow_override' => true,
        ],
        1 => [
            'type_id' => 2,
            'disk' => 'public',
            'path' => 'avatars',
            'mediatype' => 'image',
            'model' => \App\Models\User::class,
            'column' => 'avatar_id',
            'delete_unused' => true,
            'allow_override' => false,
            'max_width' => 1280,
            'is_responsive' => true,
            'min_width' => 205,
            'downsize_ratio' => 0.7,
            'thumb_width' => 120,
            'thumb_height' => 'auto',
            'thumb_format' => 'jpg'
        ],
    ],

    /**
     * documenttypes (doctypes) should be defined to easily verify if a type is a document or image
     * All non-doctypes are considered to be images
     */
    'doctypes' => [1],

    /**
     * convert images to this format
     * available: jpg, webp
     */
    'image_format' => 'jpg',

    /**
     * helper strings for the html output of a responsive image in various parts of your view files
     */

    'sizes' => [
        'aside' => '(min-width: 1280px) 420px, (min-width: 1024px) 256px, (min-width: 768px) 256px, 98vw',
        'header' => '100vw',
        'content_full' => '(min-width: 1280px) 940px, (min-width: 1024px) 746px, (min-width: 768px) 544px, 98vw',
        'content_half' => '(min-width: 1280px) 625px, (min-width: 1024px) 497px, (min-width: 768px) 485px, 98vw',
        'avatar' => '(min-width: 1280px) 200px, (min-width: 1024px) 180px, (min-width: 768px) 160px, 150px'
    ],

    'fallback_image' => [
        'disk' => 'vendor',
        'path' => 'dmedia/images/placeholder.jpg',
        'thumb_path' => 'dmedia/images/placeholder@0.25x.jpg',
    ]
];
